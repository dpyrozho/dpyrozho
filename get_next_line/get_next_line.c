/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dpyrozho <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/14 18:00:37 by dpyrozho          #+#    #+#             */
/*   Updated: 2017/12/26 11:56:38 by dpyrozho         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include "get_next_line.h"
#include <fcntl.h>
#include <string.h>
#define BUFF_SIZE 7


int 					ft_checkcontent(char **line, char **ostacha);

int				ft_givelinegivestruct(char *buf, char **line, char **ostacha)
{
	char			*cpbuf;
	int				i;
	char			perem[2];


	i = 0;
	
	if(!*ostacha)
		*ostacha = "\0";
	cpbuf = buf;
	perem[1] = '\0';
	printf("BUFFER:%s", buf);
	while (buf[i] != '\n' && buf[i])
    {
      perem[0] = buf[i];
      *line = ft_strjoin(*line, perem);
      i++;
    }
	printf("\nLINE : %s\n", *line);
    cpbuf = ft_strchr(buf, '\n');
    if (cpbuf[1])
    	*ostacha = ft_strjoin(*ostacha, ft_strchr(buf, '\n') + 1);
  	//free(buf);
	printf("OSTACHA: %s\n",*ostacha);
	if (*line)
 		return (1);
	return (0);
}


int 					ft_checkcontent(char **line, char **ostacha)
{
	char	*newst;
	int		i;
	char 	*b;
	int metka;
	
	i = 0;
	b = ft_strchr(*ostacha, '\n') + 1;

	//printf("\neto b: %li eto ostacha %li  ETO RAZNICA STR I SAMOI STROKI: %li\n",b,*ostacha, b - *ostacha);
	printf("ETO DLINA *OSTACHKI: %zu\n",ft_strlen(*ostacha));
	*line = "\0";
	newst = ft_strdup(*ostacha);
	metka = 0;
	printf("--\nETO NEWST: %s\n---", newst);
	while (newst[i])
	{
		if (newst[i] == '\n' && newst[i])
		{
			if (newst[i + 1] != '\0')
			{
				*ostacha = ft_strsub(*ostacha,b - *ostacha,ft_strlen(*ostacha));
				printf("*ostacha posle obrezki: %s\n", *ostacha);
			}
			else 
			{
				free(*ostacha);
				printf("ZAFRISHIL OSTACHU");
			}
			newst[i] = '\0';
			*line = ft_strjoin(*line, newst);
			printf("\nETO LINE: %s\n",*line);
			i++;
			metka = 1;
			break;
		}
		if (newst[i + 1] == '\0')
		{
			*line = ft_strjoin(*line, newst);
			printf("PRIVET: %s", *line);
			
			free(*ostacha);
			break;
		}
		i++;
	}
	free(newst);
	if (metka == 1)
		return(1);
	return (0);
}

int					get_next_line(int fd, char **line)
{
	char			buf[BUFF_SIZE + 1];
	int				ret;
	int i;
	static	char 	*ostacha;
	

	i = 0;
	*line = ft_strnew(1);
	if (fd < 0 || line == NULL || BUFF_SIZE <= 0)
		return (-1);
	if (fd >= 0)
	{
		if (ostacha != NULL)
			if (ft_checkcontent(line, &ostacha) == 1)
				return (1);
		while ((ret = read(fd, buf, BUFF_SIZE)) > 0 )
		{
        	buf[ret] = '\0';
        	if (ft_givelinegivestruct(buf, line, &ostacha) == 1)
        		return (1);
		}
    }
    return (1);
}

// int		main(void)
// {
// 	char		*line;
// 	int			fd;
// 	char		*filename;
	

// 	filename = "gnl7_1.txt";
// 	fd = open(filename, O_RDONLY);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);
// 	get_next_line(fd, &line);

	

// }
